//
//  Message.swift
//  Weatherfy
//
//  Created by Matiss Mamedovs on 07/05/2019.
//  Copyright © 2019 Matiss Mamedovs. All rights reserved.
//

import Foundation

enum MessageType {
    case user
    case botText
}

class Message {
    
    var text: String = ""
    var date: Date
    var type: MessageType
    
    init(date: Date, type: MessageType, message: String) {
        self.date = date
        self.type = type
        self.text = message
    }
    
    func getImage() -> String {
        switch self.type {
        case .user:
            return "user.png"
        default:
            return "bot.pdf"
        }
    }
}
