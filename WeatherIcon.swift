//
//  WeatherIcon.swift
//  Weatherfy
//
//  Created by Matiss Mamedovs on 07/05/2019.
//  Copyright © 2019 Matiss Mamedovs. All rights reserved.
//

import Foundation
import UIKit

enum WeatherIconType {
    case sun
    case cloud
    case thunder
    case snow
    case rain
    case sunCloud
    case na
}

class WeatherIcon {
    
    var code: Int = 0
    var image =  UIImage(named: "icon_na")
    var weaterIconType: WeatherIconType?

    init(id: Int) {
        self.code = id
        if id > 199 && id < 300 {
            weaterIconType = .thunder
        } else if (id > 299 && id < 400) || (id > 499 && id < 600) {
            weaterIconType = .rain
        } else if id > 599 && id < 700 {
            weaterIconType = .snow
        } else if id > 699 && id < 800 {
            weaterIconType = .rain
        } else if id == 800 {
            weaterIconType = .sun
        } else if id == 801 || id == 802 {
            weaterIconType = .sunCloud
        } else if id == 803 || id == 804 {
            weaterIconType = .cloud
        } else {
            weaterIconType = .na
        }
        
        self.image = getImage()
    }
    
    func getImage() -> UIImage {
        
        switch weaterIconType! {
        case .sun:
            return UIImage(named: "icon_sun")!
        case .cloud:
            return UIImage(named: "icon_cloud")!
        case .thunder:
            return UIImage(named: "icon_thunder")!
        case .snow:
            return UIImage(named: "icon_snow")!
        case .rain:
            return UIImage(named: "icon_rain")!
        case .sunCloud:
            return UIImage(named: "icon_sun_cloud")!
        case .na:
            return UIImage(named: "icon_na")!
        }
    }
}
