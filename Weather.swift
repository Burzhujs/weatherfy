//
//  Weather.swift
//  Weatherfy
//
//  Created by Matiss Mamedovs on 06/05/2019.
//  Copyright © 2019 Matiss Mamedovs. All rights reserved.
//

import Foundation

class Weather {
    
    var id: Int!
    var temperature: Int!
    var description: String!
    
    init(id: Int, temperature: Int, description: String) {
        self.id = id
        self.temperature = temperature
        self.description = description
    }
    
}

