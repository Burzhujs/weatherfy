//
//  ViewController.swift
//  Weatherfy
//
//  Created by Matiss Mamedovs on 06/05/2019.
//  Copyright © 2019 Matiss Mamedovs. All rights reserved.
//

import UIKit
import CoreLocation
import KeychainSwift

class ViewController: UIViewController {

    @IBOutlet weak var weathetIconImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var degreesLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var userNameInputField: UITextField!
    @IBOutlet weak var helloLabel: UILabel!
    
    var locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUserInfo()
        submitButton.layer.cornerRadius = 5.0
        submitButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(submitButtonAction)))
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) {
            setCurrentCity()
        }
        setDate()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    fileprivate func setUserInfo() {
        if let name = self.getKeychainValue("name") {
            self.submitButton.isHidden = true
            self.userNameInputField.isHidden = true
            self.helloLabel.text = "Hi, \(name)"
            self.helloLabel.isHidden = false
        } else {
            self.submitButton.isHidden = false
            self.userNameInputField.isHidden = false
            self.helloLabel.isHidden = true
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc fileprivate func submitButtonAction() {
        if userNameInputField.text?.count != 0 {
            setKeychainValue("name", value: userNameInputField.text ?? "")
            self.submitButton.isHidden = true
            self.userNameInputField.isHidden = true
            self.helloLabel.text = "Hi, \(userNameInputField.text ?? "")"
            self.helloLabel.isHidden = false
        } else {
            let alert = UIAlertController(title: "Alert", message: "Username cannot be empty!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    fileprivate func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            setCurrentCity()
        }
    }
    
    fileprivate func setDate() {
        let dateFormatterHeader = DateFormatter()
        dateFormatterHeader.setLocalizedDateFormatFromTemplate("MMMM dd yyyy")
        self.dateLabel.text = dateFormatterHeader.string(from: Date())
    }

    fileprivate func setCurrentCity() {
        guard let currentLocation = locManager.location else {
            return
        }
        
        getPlace(for: currentLocation, completion: { place in
            self.cityLabel.text = place?.locality ?? ""
        })
        APIHelper.getWeatherByCoordinates(latitude: Int(currentLocation.coordinate.latitude), longitude: Int(currentLocation.coordinate.longitude), completeCallback: { weather in
            DispatchQueue.main.async {
                self.degreesLabel.text = String(weather.temperature)
                self.degreesLabel.font = UIFont.systemFont(ofSize: 90)
                self.weatherDescriptionLabel.text = weather.description.uppercased()
                self.weathetIconImageView.image = WeatherIcon(id: weather.id).image
            }
        }, errorCallback: {
            
        })
    }
    
    fileprivate func getPlace(for location: CLLocation, completion: @escaping (CLPlacemark?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            
            guard error == nil else {
                print("*** Error in \(#function): \(error!.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let placemark = placemarks?[0] else {
                print("*** Error in \(#function): placemark is nil")
                completion(nil)
                return
            }
            
            completion(placemark)
        }
    }

    fileprivate func setKeychainValue(_ key: String, value: String?) {
        let keychain = KeychainSwift()
        if let value = value {
            keychain.set(value, forKey: key)
        } else {
            keychain.delete(key)
        }
    }
    
    fileprivate func getKeychainValue(_ key: String) -> String? {
        return KeychainSwift().get(key)
    }

}

