//
//  APIHelper.swift
//  Weatherfy
//
//  Created by Matiss Mamedovs on 06/05/2019.
//  Copyright © 2019 Matiss Mamedovs. All rights reserved.
//

import Foundation

open class APIHelper {
    
    static func getWeatherByCoordinates(latitude: Int, longitude: Int, completeCallback: @escaping (Weather) -> Void, errorCallback: @escaping () -> Void) {
        let link = "http://api.openweathermap.org/data/2.5/weather?lat=\(String(latitude))&lon=\(String(longitude))&APPID=6673638cb2e472f4beeb3644341f3eac&units=metric"
        
        let url = URL(string: link)
        var request = URLRequest(url: url!)
        
        let task = URLSession.shared.dataTask(with: request) {(data, _, error) in
            do {
                if let data = data {
                    let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject
                    print(object)
                    var weather: Weather?
                    var id: Int?
                    var temperature: Double?
                    var description: String?
                    if let array = object.value(forKey: "weather") as? NSArray {
                        if let weather = array.firstObject as? AnyObject {
                            if let weatherID = weather.value(forKey: "id") as? Int {
                                id = weatherID
                            }
                            if let weatherDescription = weather.value(forKey: "description") as? String {
                                description = weatherDescription
                            }
                        }
                    }
                    
                    if let main = object.value(forKey: "main") as? AnyObject {
                        if let temp = main.value(forKey: "temp") as? Double {
                            temperature = temp
                        }
                    }
                    
                    if let id = id, let temperature = temperature, let description = description {
                        weather = Weather(id: id, temperature: Int(temperature) ?? 0, description: description)
                        completeCallback(weather!)
                        return
                    }
                    
                } else if let err = error {
                    print(error)
                }
                //if hasnt return then error has occored on the way
                errorCallback()
            }  catch {
                
            }
        }
        
        task.resume()
    }
    
    static func getWeatherByCIty(city: String, completeCallback: @escaping (Weather) -> Void, errorCallback: @escaping () -> Void) {
        let link = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&APPID=6673638cb2e472f4beeb3644341f3eac&units=metric"
        
        guard let url = URL(string: link) else {
            errorCallback()
            return
        }
        
        var request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) {(data, _, error) in
            do {
                if let data = data {
                    let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject
                    print(object)
                    if let cod = object.value(forKey: "cod") as? String, cod == "404" {
                        errorCallback()
                        return
                    }
                    var weather: Weather?
                    var id: Int?
                    var temperature: Double?
                    var description: String?
                    if let array = object.value(forKey: "weather") as? NSArray {
                        if let weather = array.firstObject as? AnyObject {
                            if let weatherID = weather.value(forKey: "id") as? Int {
                                id = weatherID
                            }
                            if let weatherDescription = weather.value(forKey: "description") as? String {
                                description = weatherDescription
                            }
                        }
                    }
                    
                    if let main = object.value(forKey: "main") as? AnyObject {
                        if let temp = main.value(forKey: "temp") as? Double {
                            temperature = temp
                        }
                    }
                    
                    if let id = id, let temperature = temperature, let description = description {
                        weather = Weather(id: id, temperature: Int(temperature) ?? 0, description: description)
                        completeCallback(weather!)
                    }
                    
                } else if let err = error {
                    errorCallback()
                }
                //if hasnt return then error has occored on the way
            }  catch {
                errorCallback()
            }
        }
        
        task.resume()
    }
    
}
