//
//  Constants.swift
//  Weatherfy
//
//  Created by Matiss Mamedovs on 13/06/2019.
//  Copyright © 2019 Matiss Mamedovs. All rights reserved.
//

import Foundation

public let errorMessages: [String] = ["Sorry, I didn´t get that, could you rephrase?","Whops, please try again 😞","Are you sure that´s a real place?"]
