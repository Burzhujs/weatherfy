//
//  ChatViewController.swift
//  Weatherfy
//
//  Created by Matiss Mamedovs on 07/05/2019.
//  Copyright © 2019 Matiss Mamedovs. All rights reserved.
//

import UIKit
import KeychainSwift

class ChatViewController: UIViewController {
    
    //Messages
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    @IBOutlet weak var sendButton: UIBarButtonItem!
    
    var lastCityName: String = ""
    
    var messages: [Message] = []
    var failMessages: [String] = errorMessages
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        setupBackButton()
        
        sendWelcomeMessage()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.sendSecondMessage()
        }
    }
    
    fileprivate func setupBackButton() {
        backButton.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        backButton.clipsToBounds = true
        backButton.layer.cornerRadius = 25
        backButton.setImage(UIImage(named: "icon_close"), for: .normal)
        backButton.addTarget(self, action: #selector(backHome), for: .touchUpInside)
    }
    
    fileprivate func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "UserTableViewCell")
        tableView.register(UINib(nibName: "TextResponseTableViewCell", bundle: nil), forCellReuseIdentifier: "TextResponseTableViewCell")
        tableView.register(UINib(nibName: "ForecastResponseTableViewCell", bundle: nil), forCellReuseIdentifier: "ForecastResponseTableViewCell")
        tableView.estimatedRowHeight = 56
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        
        tableView.tableFooterView = UIView()
    }
    
    @objc final func keyboardWillShow(notification: Notification) {
        moveToolbar(up: true, notification: notification)
    }
    
    @objc final func keyboardWillHide(notification: Notification) {
        moveToolbar(up: false, notification: notification)
    }
    
    final func moveToolbar(up: Bool, notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }

        let height = keyboardSize.cgRectValue.height
        
        if up {
            self.textView.frame.origin.y -= height
            self.toolbar.frame.origin.y -= height
        } else {
            self.textView.frame.origin.y += height
            self.toolbar.frame.origin.y += height
        }
        
    }

    @objc func backHome() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonAction(_ sender: Any) {
        if textView.text != "" {
            self.lastCityName = textView.text
            let message = Message(date: Date(), type: .user, message: lastCityName)
            self.sendMessage(message)
            self.textView.text = ""
            APIHelper.getWeatherByCIty(city: lastCityName, completeCallback: weatherReturned, errorCallback: weatherFailed)
        }
        self.view.endEditing(true)
    }
    
    fileprivate func weatherReturned(weather: Weather) {
        DispatchQueue.main.async {
            let responseText = "The temperature in \(self.lastCityName) is \(String(weather.temperature)) degrees and its \(String(weather.description)) right now."
            let message = Message(date: Date(), type: .botText, message: responseText)
            self.sendMessage(message)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                let message = Message(date: Date(), type: .botText, message: "Do you want me to check another place?")
                self.sendMessage(message)
            }
        }        
    }
    
    fileprivate func weatherFailed() {
        DispatchQueue.main.async {
            let text = self.failMessages.randomElement() ?? ""
            
            let message = Message(date: Date(), type: .botText, message: text)
            self.sendMessage(message)
        }
    }
    
    func sendMessage(_ message: Message) {
        messages.append(message)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: messages.count - 1, section: 0)], with: .automatic)
        tableView.endUpdates()
        
        let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    func sendWelcomeMessage() {
        let firstTime = true
        if firstTime {
            var name = ""
            if let userName = getKeychainValue("name") {
                name += " "
                name += userName
            }
            let text = "Oh hello again\(name).I hope you’ve been well 🙂 "
            let message = Message(date: Date(), type: .botText, message: text)
            messages.append(message)
        }
    }
    
    func sendSecondMessage() {
        let text = "Give me a name of a city and I will check the weather for you "
        let message = Message(date: Date(), type: .botText, message: text)
        messages.append(message)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: messages.count - 1, section: 0)], with: .automatic)
        tableView.endUpdates()
    }
    
    fileprivate func setKeychainValue(_ key: String, value: String?) {
        let keychain = KeychainSwift()
        if let value = value {
            keychain.set(value, forKey: key)
        } else {
            keychain.delete(key)
        }
    }
    
    fileprivate func getKeychainValue(_ key: String) -> String? {
        return KeychainSwift().get(key)
    }

}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        
        switch message.type {
        case .user:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
            cell.loadData(with: message)
            return cell
        case .botText:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextResponseTableViewCell", for: indexPath) as! TextResponseTableViewCell
            cell.loadData(with: message)
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !tableView.isDecelerating {
            view.endEditing(true)
        }
    }
    
}
